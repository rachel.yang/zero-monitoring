(function () {
  'use strict';

  angular
    .module('zeroWeb')
    .config(config);

  /** @ngInject */
  function config($logProvider, $urlRouterProvider, jwtInterceptorProvider, $httpProvider) {
    // Enable log
    $logProvider.debugEnabled(true);

    $urlRouterProvider.otherwise('/');

    jwtInterceptorProvider.tokenGetter = function(store) {
      return store.get('jwt');
    };

    $httpProvider.interceptors.push('jwtInterceptor');

  }

})();
