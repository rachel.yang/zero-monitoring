(function () {
  'use strict';

  angular
    .module('zeroWeb')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider.state('app', {
        abstract: true,
        url: '',
        template: '<ui-view></ui-view>'
      }
    );

    $stateProvider
      .state('home', {
        url: '/home',
        templateUrl: 'app/components/main/home/home.html',
        controller: 'HomeController',
        controllerAs: 'vm'
      })
      .state('login', {
        url: '/',
        templateUrl: 'app/components/main/login/login.html',
        controller: 'LoginController',
        controllerAs: 'vm'
      })
      .state('signup', {
        url: '/signup',
        templateUrl: 'app/components/main/signup/signup.html',
        controller: 'SignupController',
        controllerAs: 'vm'
      })
      .state('userinfo', {
        url: '/userinfo',
        templateUrl: 'app/components/main/userinfo/userinfo.html',
        controller: 'UserinfoController',
        controllerAs: 'vm'
      })
    ;

    $urlRouterProvider.otherwise('/');
  }

})();
