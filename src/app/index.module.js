(function () {
  'use strict';

  angular
    .module('zeroWeb',
      [
        'ngAnimate',
        'ngResource',
        'ui.router',
        'ui.bootstrap',
        'toastr',
        'zeroWeb-sidebar',
        'ngCookies',
        'angular-jwt',
        'angular-storage',
        'angular-svg-round-progress']);

})();
