(function() {
  'use strict';

  angular
    .module('zeroWeb')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log, $rootScope, $state, store, jwtHelper, loginService) {
    $log.debug('runBlock end');

    var stateChange = $rootScope.$on('$stateChangeStart', function(e, to) {
      if (to.data && to.data.requiresLogin) {
        if (!store.get('jwt') || jwtHelper.isTokenExpired(store.get('jwt'))) {
          e.preventDefault();
          $state.go('login');
        }
      }
    });

    $rootScope.$on('$destroy', stateChange);

    //navbar user info
    store.storage.jwt = store.get('jwt');
    $rootScope.user = store.storage.jwt && jwtHelper.decodeToken(store.storage.jwt);
    $log.info("After refresh, current user is :: ", $rootScope.user);

    $rootScope.logout = function () {
      loginService.logoutUser();
    };

    $rootScope.clickLogo = function() {
      if (!store.get('jwt') || jwtHelper.isTokenExpired(store.get('jwt'))) {
        $state.go('login');
      } else {
        $state.go("sidebar.monitoring.m1");
      }
    }
  }

})();
