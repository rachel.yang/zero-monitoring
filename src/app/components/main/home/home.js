/**
 * Created by rachel.yang on 2016. 1. 7..
 */
(function () {
  'use strict';

  angular
    .module('zeroWeb')
    .factory('loginService', loginService)
    .controller('HomeController', HomeController);

  function loginService($log, $http, $q, store, $state, $rootScope, jwtHelper) {
    /**
     * High level, public methods
     */
    var wrappedService = {

        loginUser: function (loginParams) {
          $log.info('loginService : loginUser');
          var loginDefer = $q.defer();

          $http({
            method: 'POST',
            url: 'http://61.39.74.111:32770/login',
            data: loginParams
            //headers: {'Authorization': 'JWT ' + token}
          }).then(
            function (resp) {
              store.set('jwt', resp.data.data.token);
              angular.extend(wrappedService.user, resp.data.data);
              wrappedService.isLogged = true;

              store.storage.jwt = store.get('jwt');
              $rootScope.user = store.storage.jwt && jwtHelper.decodeToken(store.storage.jwt);

              loginDefer.resolve(resp.data.data);

            },
            function (reason) {
              $log.error("error: ", reason.data.message);
              loginDefer.reject(reason.data.message);
            }
          );
          return loginDefer.promise;
        },

        /*currentUser: function () {
         Session.get(function (user) {
         $rootScope.currentUser = user;
         });
         },*/

        logoutUser: function () {
          $log.info('loginService : logoutUser');
          store.set('jwt', null);
          $rootScope.user = null;
          $state.go("home");
          $state.reload();
        },

        /**
         * Public properties
         */
        user: {},
        domain: {},
        isLogged: null
      };

    return wrappedService;
  }//  loginService end

  /** @ngInject */
  function HomeController($log, $rootScope, $http) {
    var vm = this;
    $log.info("HomeController start");

    //$state.go('sidebar.monitoring.m1');

    //store.starage.jwt = store.get('jwt');
    //$rootScope.user = store.starage.jwt && jwtHelper.decodeToken(store.starage.jwt);

    vm.user = $rootScope.user;

    //url 인증 예시
    vm.callAnonymousApi = function () {
      // Just call the API as you'd do using $http
      callApi('Anonymous', 'http://localhost:3000/api/random-quote');
    };

    vm.callSecuredApi = function () {
      callApi('Secured', 'http://localhost:3000/api/protected/random-quote');
    };

    function callApi(type, url) {
      vm.response = null;
      vm.api = type;
      $http({
        url: url,
        method: 'GET'
      }).then(function (quote) {
        vm.response = quote.data;
      }, function (error) {
        vm.response = error.data;
      });
    }
  }
})();
