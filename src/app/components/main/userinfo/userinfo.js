/**
 * Created by rachel.yang on 2016. 1. 12..
 */
(function () {
  'use strict';

  angular
    .module('zeroWeb')
    .controller('UserinfoController', UserinfoController);

  /** @ngInject */
  function UserinfoController($log, $scope, $http, $state, store, $rootScope, jwtHelper) {
    var vm = this;

    $log.info("UserinfoController start");

    vm.user = $rootScope.user;

    vm.role = [
      {name: 'admin'},
      {name: 'user'},
      {name: 'guest'}
    ];

    vm.edituserinfo = function () {

      var userinfoParams = {
        "id": $scope.user.id,
        "name": $scope.user.name,
        "password": $scope.password,
        "email": $scope.user.email,
        "role": $scope.user.role
      };

      $http({
        method: 'PUT',
        url: 'http://61.39.74.111:32770/users/' + $scope.user.id,
        data: userinfoParams,
        headers: {'Authorization': store.storage.jwt}
      }).then(
        function (resp) {
          $log.info("Success Edit userinfo ::", resp.data);

          //token 갱신
          store.set('jwt', resp.data.data.token);
          store.storage.jwt = store.get('jwt');
          $rootScope.user = store.storage.jwt && jwtHelper.decodeToken(store.storage.jwt);

          alert('변경되었습니다.');
          $state.go("home");
        },
        function (reason) {
          $log.error("error: ", reason.data);
          alert(reason.data);
        }
      );
    };
  }
})();
