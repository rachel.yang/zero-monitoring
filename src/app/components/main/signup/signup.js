/**
 * Created by yangyoomi on 2015. 12. 22..
 */
(function () {
  'use strict';

  angular
    .module('zeroWeb')
    .controller('SignupController', SignupController);

  /** @ngInject */
  function SignupController($log, $http, $state, store, $rootScope, jwtHelper) {
    var vm = this;

    $log.info("SignupController start");

    vm.role = [
      {name: 'admin'},
      {name: 'user'},
      {name: 'guest'}
    ];

    vm.signup = function () {

      var signupParams = {
        "id": vm.id,
        "name": vm.name,
        "password": vm.password,
        "email": vm.email,
        "role": vm.role.userSelect
      };

      $http({
        method: 'POST',
        url: 'http://61.39.74.111:32770/register',
        data: signupParams
        //headers: {'Content-Type': 'application/json'}
      }).then(
        function (resp) {
          store.set('jwt', resp.data.data.token);

          //auth
          store.storage.jwt = store.get('jwt');
          $rootScope.user = store.storage.jwt && jwtHelper.decodeToken(store.storage.jwt);

          $log.info("Signup Success!!! & Login");
          $state.go("sidebar.monitoring.m1");
        },
        function (reason) {
          $log.error("error: ", reason.data.message.message);
          alert(reason.data.message.message);
        }
      );
    };
  }
})();
