/**
 * Created by yangyoomi on 2015. 12. 15..
 */
(function () {
  'use strict';

  angular
    .module('zeroWeb')
    .controller('LoginController', LoginController);

  /** @ngInject */
  function LoginController($log, $state, loginService) {
    var vm = this;

    $log.info("LoginController start");

    vm.login = function () {

      var loginParams = {
        "id": vm.id,
        "password": vm.password
      };

      loginService.loginUser(loginParams).then(
        function (resp) {
          $log.info("Login Success!!", resp);
          $state.go("sidebar.monitoring.m1");
        },
        function (reason) {
          alert(reason);
        }
      );

    };
  }
})();
