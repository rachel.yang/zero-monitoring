/**
 * Created by rachel.yang on 2016. 2. 14..
 */
(function () {
  'use strict';

  angular
    .module('zeroWeb-monitoring')
    .controller('M2Controller', M2Controller);

  /** @ngInject */
  function M2Controller($log, $interval, moment, c3, $http) {
    $log.info("M2Controller start");
    var vm = this;

    $http.get('http://61.39.74.111:32770/scr_energyload_kwh_15m')
      .then(function(resp) {
        $log.info('all data:: ', resp.data.data);
        vm.data = resp.data.data;

        vm.f_all = [];

        vm.f_cooling = ['냉방'];
        vm.f_etc = ['생활'];
        vm.f_heating = ['난방'];
        vm.f_hotwater = ['급탕'];
        vm.f_lighting = ['조명'];
        vm.f_venting = ['환기'];

        for (var i=0; i<vm.data.length; i++) {
          vm.f_all.push(vm.data[i].f_all);
          vm.f_cooling.push(vm.data[i].f_cooling);
          vm.f_etc.push(vm.data[i].f_etc);
          vm.f_heating.push(vm.data[i].f_heating);
          vm.f_hotwater.push(vm.data[i].f_hotwater);
          vm.f_lighting.push(vm.data[i].f_lighting);
          vm.f_venting.push(vm.data[i].f_venting);
        }

        //Calculate table value
        vm.day_all = 0;
        for (var i=1; i<vm.f_all.length; i++) {
          vm.day_all = vm.day_all + vm.f_all[i];
        }
        vm.day_all_avg = vm.day_all/vm.f_all.length;
        $log.info("전체 당일사용량:: ", vm.day_all);

        vm.day_cooling = 0;
        for (var i=1; i<vm.f_cooling.length; i++) {
          vm.day_cooling = vm.day_cooling + vm.f_cooling[i];
        }
        $log.info("냉방 당일사용량:: ", vm.day_cooling);

        vm.day_heating = 0;
        for (var i=1; i<vm.f_heating.length; i++) {
          vm.day_heating = vm.day_heating + vm.f_heating[i];
        }
        $log.info("난방 당일사용량:: ", vm.day_heating);

        vm.day_hotwater = 0;
        for (var i=1; i<vm.f_hotwater.length; i++) {
          vm.day_hotwater = vm.day_hotwater + vm.f_hotwater[i];
        }
        $log.info("급탕 당일사용량:: ", vm.day_hotwater);

        vm.day_lighting = 0;
        for (var i=1; i<vm.f_lighting.length; i++) {
          vm.day_lighting = vm.day_lighting + vm.f_lighting[i];
        }
        $log.info("조명 당일사용량:: ", vm.day_lighting);

        vm.day_venting = 0;
        for (var i=1; i<vm.f_venting.length; i++) {
          vm.day_venting = vm.day_venting + vm.f_venting[i];
        }
        $log.info("환기 당일사용량:: ", vm.day_venting);

        vm.day_etc = 0;
        for (var i=1; i<vm.f_etc.length; i++) {
          vm.day_etc = vm.day_etc + vm.f_etc[i];
        }
        $log.info("생활 당일사용량:: ", vm.day_etc);

        vm.fifthLoad = vm.day_cooling + vm.day_heating + vm.day_hotwater + vm.day_lighting + vm.day_venting;
        vm.fifthLoadAvg = vm.fifthLoad/vm.data.length;


        for (var i=0; i< 96-vm.data.length; i++) {
          vm.f_cooling.push(0);
          vm.f_heating.push(0);
          vm.f_hotwater.push(0);
          vm.f_lighting.push(0);
          vm.f_venting.push(0);
          vm.f_etc.push(0);
        }

        vm.x = ['x',
          '00:00', '00:15', '00:30', '00:45', '01:00', '01:15', '01:30', '01:45',
          '02:00', '02:15', '02:30', '02:45', '03:00', '03:15', '03:30', '03:45',
          '04:00', '04:15', '04:30', '04:45', '05:00', '05:15', '05:30', '05:45',
          '06:00', '06:15', '06:30', '06:45', '07:00', '07:15', '07:30', '07:45',
          '08:00', '08:15', '08:30', '08:45', '09:00', '09:15', '09:30', '09:45',
          '10:00', '10:15', '10:30', '10:45', '11:00', '11:15', '11:30', '11:45',
          '12:00', '12:15', '12:30', '12:45', '13:00', '13:15', '13:30', '13:45',
          '14:00', '14:15', '14:30', '14:45', '15:00', '15:15', '15:30', '15:45',
          '16:00', '16:15', '16:30', '16:45', '17:00', '17:15', '17:30', '17:45',
          '18:00', '18:15', '18:30', '18:45', '19:00', '19:15', '19:30', '19:45',
          '20:00', '20:15', '20:30', '20:45', '21:00', '21:15', '21:30', '21:45',
          '23:00', '23:15', '23:30', '23:45', '24:00', '24:15', '24:30', '24:45'
        ];

        //m2 stacked bar chart
        c3.generate({
          bindto: '#m2_bar',
          data: {
            x: vm.x[0],
            xFormat:'%H:%M',
            columns: [vm.x, vm.f_cooling, vm.f_heating, vm.f_hotwater, vm.f_lighting, vm.f_venting, vm.f_etc],
            type: 'bar',
            groups: [
              ['냉방', '난방', '급탕', '조명', '환기', '생활', '모니터링']
            ]
          },
          axis: {
            x: {
              type: 'timeseries',
              tick: {
                format: '%H:%M',
                values: ['00:00', '03:00', '06:00', '09:00', '12:00', '15:00', '18:00', '21:00', '24:00']
              }
            }
          },
          grid: {
            y: {
              lines: [{value:0}]
            }
          },
          size: {
            width: 1660,
            height: 300
          },
          bar: {
            width: {
              ratio: 0.05
            }
          },
          color: {
            pattern: ['#6fd7fb', '#49b8af', '#fec271', '#954192', '#c35a20', '#e2bbe1', '#56b7c2']
          },
          legend: {
            show: false
          }
        });


      });



    $http.get('http://61.39.74.111:32770/scr_energyload_kwh_15m/last')
    .then(function(resp) {
      $log.info('last data:: ', resp.data.data);
      vm.last = resp.data.data;

      vm.last_d_date = vm.last.d_date;
      vm.last_d_updatetime = vm.last.d_updatetime;

      vm.current = moment(vm.last_d_updatetime).format('YYYY MMMM Do, a h:mm');
      $interval(function() {
        vm.current = moment().format('YYYY MMMM Do, a h:mm');
      }, 900000);


      vm.last_f_all = vm.last.f_all;
      vm.last_f_cooling = vm.last.f_cooling;
      vm.last_f_etc = vm.last.f_etc;
      vm.last_f_heating = vm.last.f_heating;
      vm.last_f_hotwater = vm.last.f_hotwater;
      vm.last_f_lighting = vm.last.f_lighting;
      vm.last_f_venting = vm.last.f_venting;
      vm.last_n_scr_energyload_kwh_15mID = vm.last.n_scr_energyload_kwh_15mID;
      vm.last_n_timeset = vm.last.n_timeset;
      vm.last_n_vpsID = vm.last.n_vpsID;
      vm.last_s_parameter = vm.last.s_parameter;
      vm.last_s_vpsname = vm.last.s_vpsname;

      //for table rate
      vm.coolingWidth = vm.last_f_cooling == 0 ? 0 : vm.last_f_cooling/vm.last_f_all *100;
      vm.heatingWidth = vm.last_f_heating == 0 ? 0 : vm.last_f_heating/vm.last_f_all *100;
      vm.hotwaterWidth = vm.last_f_hotwater == 0 ? 0 : vm.last_f_hotwater/vm.last_f_all *100;
      vm.lightingWidth = vm.last_f_lighting == 0 ? 0 : vm.last_f_lighting/vm.last_f_all *100;
      vm.ventingWidth = vm.last_f_venting == 0 ? 0 : vm.last_f_venting/vm.last_f_all *100;
      vm.etcWidth = vm.last_f_etc == 0 ? 0 : vm.last_f_etc/vm.last_f_all *100;


      //vm.last_all = vm.last_f_cooling + vm.last_f_heating + vm.last_f_hotwater + vm.last_f_lighting + vm.last_f_venting;
      //START :: donut
      vm.donutWidth = 13;
      vm.donutLabel = {
        show: false
      };
      vm.donutSize = {
        width: 160,
        height: 160
      };
      vm.donutLegend = {
        show: false
      };
      vm.donutTooltip = {
        show: false
      };

      //donut chart 5th
      c3.generate({
        bindto: '#m2_donut1',
        data: {
          columns: [
            ['data1', vm.last_f_all == 0 ? 100 : vm.last_f_all - vm.last_f_cooling],
            ['냉방', vm.last_f_cooling]
          ],
          type : 'donut',
          onclick: function (d, i) { $log.info("onclick", d, i); }
        },
        donut: {
          title: vm.last_f_cooling == 0 ? 0 + '%' : vm.last_f_cooling/vm.last_f_all * 100 + '%',
          label: vm.donutLabel,
          width: vm.donutWidth
        },
        color: {
          pattern: ['#d9d9d9', '#6fd7fb']
        },
        size: vm.donutSize,
        legend: vm.donutLegend,
        tooltip: vm.donutTooltip
      });

      c3.generate({
        bindto: '#m2_donut2',
        data: {
          columns: [
            ['data1', vm.last_f_all == 0 ? 100 : vm.last_f_all - vm.last_f_heating],
            ['난방', vm.last_f_heating]
          ],
          type : 'donut',
          onclick: function (d, i) { $log.info("onclick", d, i); }
        },
        donut: {
          title: vm.last_f_heating == 0 ? 0 + '%' : vm.last_f_heating/vm.last_f_all * 100 + '%',
          label: vm.donutLabel,
          width: vm.donutWidth
        },
        color: {
          pattern: ['#d9d9d9', '#49b8af']
        },
        size: vm.donutSize,
        legend: vm.donutLegend,
        tooltip: vm.donutTooltip
      });
      c3.generate({
        bindto: '#m2_donut3',
        data: {
          columns: [
            ['data1', vm.last_f_all == 0 ? 100 : vm.last_f_all - vm.last_f_hotwater],
            ['급탕', vm.last_f_hotwater]
          ],
          type : 'donut',
          onclick: function (d, i) { $log.info("onclick", d, i); }
        },
        donut: {
          title: vm.last_f_hotwater == 0 ? 0 + '%' : vm.last_f_hotwater/vm.last_f_all * 100 + '%',
          label: vm.donutLabel,
          width: vm.donutWidth
        },
        color: {
          pattern: ['#d9d9d9', '#fec271']
        },
        size: vm.donutSize,
        legend: vm.donutLegend,
        tooltip: vm.donutTooltip
      });
      c3.generate({
        bindto: '#m2_donut4',
        data: {
          columns: [
            ['data1', vm.last_f_all == 0 ? 100 : vm.last_f_all - vm.last_f_lighting],
            ['조명', vm.last_f_lighting]
          ],
          type : 'donut',
          onclick: function (d, i) { $log.info("onclick", d, i); }
        },
        donut: {
          title: vm.last_f_lighting == 0 ? 0 + '%' : vm.last_f_lighting/vm.last_f_all * 100 + '%',
          label: vm.donutLabel,
          width: vm.donutWidth
        },
        color: {
          pattern: ['#d9d9d9', '#954192']
        },
        size: vm.donutSize,
        legend: vm.donutLegend,
        tooltip: vm.donutTooltip
      });
      c3.generate({
        bindto: '#m2_donut5',
        data: {
          columns: [
            ['data1', vm.last_f_all == 0 ? 100 : vm.last_f_all - vm.last_f_venting],
            ['환기', vm.last_f_venting]
          ],
          type : 'donut',
          onclick: function (d, i) { $log.info("onclick", d, i); }
        },
        donut: {
          title: vm.last_f_venting == 0 ? 0 + '%' : vm.last_f_venting/vm.last_f_all * 100 + '%',
          label: vm.donutLabel,
          width: vm.donutWidth
        },
        color: {
          pattern: ['#d9d9d9', '#c35a20']
        },
        size: vm.donutSize,
        legend: vm.donutLegend,
        tooltip: vm.donutTooltip
      });

      //END:: donut
    });


  }
})();
