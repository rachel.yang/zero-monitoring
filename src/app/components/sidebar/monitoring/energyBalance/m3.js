/**
 * Created by rachel.yang on 2016. 2. 14..
 */
(function () {
  'use strict';

  angular
    .module('zeroWeb-monitoring')
    .controller('M3Controller', M3Controller);

  /** @ngInject */
  function M3Controller($log, c3, $http, $interval, moment) {
    $log.info("M3Controller start");
    var vm = this;

    $http.get('http://61.39.74.111:32770/scr_energyprod_kwh_15m')
      .then(function (resp) {
        $log.info('all data:: ', resp.data.data);
        vm.data = resp.data.data;

        vm.f_all = [];

        vm.f_inv1 = ['옥상'];
        vm.f_inv2 = ['서측'];
        vm.f_inv3 = ['남측'];

        for (var i = 0; i < vm.data.length; i++) {
          vm.f_all.push(vm.data[i].f_all);
          vm.f_inv1.push(vm.data[i].f_inv1);
          vm.f_inv2.push(vm.data[i].f_inv2);
          vm.f_inv3.push(vm.data[i].f_inv3);
        }

        //Calculate table value
        vm.day_all = 0;
        for (var i = 1; i < vm.f_all.length; i++) {
          vm.day_all = vm.day_all + vm.f_all[i];
        }
        vm.day_all_avg = vm.day_all / vm.f_all.length;
        $log.info("전체 당일사용량:: ", vm.day_all);

        vm.day_inv1 = 0;
        for (var i = 1; i < vm.f_inv1.length; i++) {
          vm.day_inv1 = vm.day_inv1 + vm.f_inv1[i];
        }
        $log.info("옥상 당일사용량:: ", vm.day_inv1);

        vm.day_inv2 = 0;
        for (var i = 1; i < vm.f_inv2.length; i++) {
          vm.day_inv2 = vm.day_inv2 + vm.f_inv2[i];
        }
        $log.info("서측 당일사용량:: ", vm.day_inv2);

        vm.day_inv3 = 0;
        for (var i = 1; i < vm.f_inv3.length; i++) {
          vm.day_inv3 = vm.day_inv3 + vm.f_inv3[i];
        }
        $log.info("남측 당일사용량:: ", vm.day_inv3);


        for (var i=0; i< 96-vm.data.length; i++) {
          vm.f_inv1.push(0);
          vm.f_inv2.push(0);
          vm.f_inv3.push(0);
        }

        vm.x = ['x',
          '00:00', '00:15', '00:30', '00:45', '01:00', '01:15', '01:30', '01:45',
          '02:00', '02:15', '02:30', '02:45', '03:00', '03:15', '03:30', '03:45',
          '04:00', '04:15', '04:30', '04:45', '05:00', '05:15', '05:30', '05:45',
          '06:00', '06:15', '06:30', '06:45', '07:00', '07:15', '07:30', '07:45',
          '08:00', '08:15', '08:30', '08:45', '09:00', '09:15', '09:30', '09:45',
          '10:00', '10:15', '10:30', '10:45', '11:00', '11:15', '11:30', '11:45',
          '12:00', '12:15', '12:30', '12:45', '13:00', '13:15', '13:30', '13:45',
          '14:00', '14:15', '14:30', '14:45', '15:00', '15:15', '15:30', '15:45',
          '16:00', '16:15', '16:30', '16:45', '17:00', '17:15', '17:30', '17:45',
          '18:00', '18:15', '18:30', '18:45', '19:00', '19:15', '19:30', '19:45',
          '20:00', '20:15', '20:30', '20:45', '21:00', '21:15', '21:30', '21:45',
          '23:00', '23:15', '23:30', '23:45', '24:00', '24:15', '24:30', '24:45'
        ];

        //m3 stacked bar chart
        c3.generate({
          bindto: '#m3_bar',
          data: {
            x: vm.x[0],
            xFormat:'%H:%M',
            columns: [vm.x, vm.f_inv1, vm.f_inv2, vm.f_inv3],
            type: 'bar',
            groups: [
              ['옥상', '남측', '서측']
            ]
          },
          axis: {
            x: {
              type: 'timeseries',
              tick: {
                format: '%H:%M',
                values: ['00:00', '03:00', '06:00', '09:00', '12:00', '15:00', '18:00', '21:00', '24:00']
              }
            }
          },
          grid: {
            y: {
              lines: [{value: 0}]
            }
          },
          size: {
            width: 1660,
            height: 300
          },
          bar: {
            width: {
              ratio: 0.05
            }
          },
          color: {
            pattern: ['#c35a20', '#e2bbe1', '#56b7c2']
          },
          legend: {
            show: false
          }
        });

      });


    $http.get('http://61.39.74.111:32770/scr_energyprod_kwh_15m/last')
      .then(function (resp) {
        $log.info('last data:: ', resp.data.data);
        vm.last = resp.data.data;

        vm.last_d_date = vm.last.d_date;
        vm.last_d_updatetime = vm.last.d_updatetime;

        vm.current = moment(vm.last_d_updatetime).format('YYYY MMMM Do, a h:mm');
        $interval(function() {
          vm.current = moment().format('YYYY MMMM Do, a h:mm');
        }, 900000);


        vm.last_f_all = vm.last.f_all;
        vm.last_f_inv1 = vm.last.f_inv1;
        vm.last_f_inv2 = vm.last.f_inv2;
        vm.last_f_inv3 = vm.last.f_inv3;

        //for table rate
        vm.inv1Width = vm.last_f_inv1 == 0 ? 0 : vm.last_f_inv1/vm.last_f_all *100;
        vm.inv2Width = vm.last_f_inv2 == 0 ? 0 : vm.last_f_inv2/vm.last_f_all *100;
        vm.inv3Width = vm.last_f_inv3 == 0 ? 0 : vm.last_f_inv3/vm.last_f_all *100;


        vm.donutWidth = 13;
        vm.donutLabel = {
          show: false
        };
        vm.donutSize = {
          width: 160,
          height: 160
        };
        vm.donutLegend = {
          show: false
        };
        vm.donutTooltip = {
          show: false
        };

        //donut chart 5th
        c3.generate({
          bindto: '#m3_donut1',
          data: {
            columns: [
              ['data1', vm.last_f_all == 0 ? 100 : 1 - vm.last_f_all],
              ['단지전체', vm.last_f_all]
            ],
            type: 'donut',
            onclick: function (d, i) {
              $log.info("onclick", d, i);
            }
          },
          donut: {
            title: vm.last_f_all == 0 ? 0 + '%' : Number(vm.last_f_all * 100).toFixed(1) + '%',
            label: vm.donutLabel,
            width: vm.donutWidth
          },
          color: {
            pattern: ['#d9d9d9', '#6fd7fb']
          },
          size: vm.donutSize,
          legend: vm.donutLegend,
          tooltip: vm.donutTooltip
        });

        c3.generate({
          bindto: '#m3_donut2',
          data: {
            columns: [
              ['data1', vm.last_f_all == 0 ? 100 : 1 - vm.last_f_all],
              ['건물전체', vm.last_f_all]
            ],
            type: 'donut',
            onclick: function (d, i) {
              $log.info("onclick", d, i);
            }
          },
          donut: {
            title: vm.last_f_all == 0 ? 0 + '%' : Number(vm.last_f_all * 100).toFixed(1) + '%',
            label: vm.donutLabel,
            width: vm.donutWidth
          },
          color: {
            pattern: ['#d9d9d9', '#49b8af']
          },
          size: vm.donutSize,
          legend: vm.donutLegend,
          tooltip: vm.donutTooltip
        });
        c3.generate({
          bindto: '#m3_donut3',
          data: {
            columns: [
              ['data1', vm.last_f_all == 0 ? 100 : vm.last_f_all - vm.last_f_inv1],
              ['옥상', vm.last_f_inv1]
            ],
            type: 'donut',
            onclick: function (d, i) {
              $log.info("onclick", d, i);
            }
          },
          donut: {
            title: vm.last_f_inv1 == 0 ? 0 + '%' : Number(vm.last_f_inv1/vm.last_f_all * 100).toFixed(1) + '%',
            label: vm.donutLabel,
            width: vm.donutWidth
          },
          color: {
            pattern: ['#d9d9d9', '#fec271']
          },
          size: vm.donutSize,
          legend: vm.donutLegend,
          tooltip: vm.donutTooltip
        });
        c3.generate({
          bindto: '#m3_donut4',
          data: {
            columns: [
              ['data1', vm.last_f_all == 0 ? 100 : vm.last_f_all - vm.last_f_inv3],
              ['서측', vm.last_f_inv3]
            ],
            type: 'donut',
            onclick: function (d, i) {
              $log.info("onclick", d, i);
            }
          },
          donut: {
            title: vm.last_f_inv3 == 0 ? 0 + '%' : Number(vm.last_f_inv3/vm.last_f_all * 100).toFixed(1) + '%',
            label: vm.donutLabel,
            width: vm.donutWidth
          },
          color: {
            pattern: ['#d9d9d9', '#954192']
          },
          size: vm.donutSize,
          legend: vm.donutLegend,
          tooltip: vm.donutTooltip
        });
        c3.generate({
          bindto: '#m3_donut5',
          data: {
            columns: [
              ['data1', vm.last_f_all == 0 ? 100 : vm.last_f_all - vm.last_f_inv2],
              ['남측', vm.last_f_inv2]
            ],
            type: 'donut',
            onclick: function (d, i) {
              $log.info("onclick", d, i);
            }
          },
          donut: {
            title: vm.last_f_inv2 == 0 ? 0 + '%' : Number(vm.last_f_inv2/vm.last_f_all * 100).toFixed(1) + '%',
            label: vm.donutLabel,
            width: vm.donutWidth
          },
          color: {
            pattern: ['#d9d9d9', '#c35a20']
          },
          size: vm.donutSize,
          legend: vm.donutLegend,
          tooltip: vm.donutTooltip
        });

      });


  }
})();
