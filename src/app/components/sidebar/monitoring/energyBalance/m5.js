/**
 * Created by rachel.yang on 2016. 2. 15..
 */
(function () {
  'use strict';

  angular
    .module('zeroWeb-monitoring')
    .controller('M5Controller', M5Controller);

  /** @ngInject */
  function M5Controller($log, c3, $http) {
    $log.info("M5Controller start");

    var vm = this;

    $http.get('http://61.39.74.111:32770/indoor1').then(function(response) {
      var data = response.data.data;
      var last = data[data.length -1];

      $log.info(data);
      vm.intemp = last.Temperature;
      vm.inhum = last.Humidity;
      vm.inco2 = last.CO2;
      vm.inpress = last.Pressure;

      chart.load({
        json: data,
        keys: {
          value: ['Temperature']
        }
      })
    });

    //m2 stacked bar chart
    var chart = c3.generate({
      bindto: '#m5_line',
      data: {
        columns: [
        ]
      },

      size: {
        width: 1660,
        height: 200
      },
      bar: {
        width: {
          ratio: 0.3
        }
      },
      color: {
        pattern: ['#c35a20', '#e2bbe1', '#56b7c2']
      },
      legend: {
        show: false
      },
      axis: {
        y: {
          show: true
        }
      }
    });

  }
})();
