(function () {
  'use strict';

  angular
    .module('zeroWeb-monitoring')
    .controller('M1Controller', M1Controller);

  /** @ngInject */
  function M1Controller($log, $http, c3) {
    $log.info("M1Controller start");

    var vm = this;

    var chart = c3.generate({
      bindto: '#chart-line',
      data: {
        columns: [],
        type: 'line'
      },
      axis: {
        y: {
          show: false
        },
        x: {
          show: false
        }
      },
      size: {
        width: 1100,
        height: 200
      },
      padding: {
        top: 10
      },
      color: {
        pattern: ['#27ADF5', '#BE0612']
      },
      legend: {
        show: false
      },
      point: {
        show: false
      },
      grid: {
        x: {
          show: true
        }
      },
      interaction: {
        enabled: false
      }
    });

    $http.get('http://61.39.74.111:32770/trendelecbalancetoday').then(function (response) {
      var data = response.data.data;
      var last = data[data.length - 1];

      vm.buildings = [];

      var mockup = {};

      mockup.name = '목업주택';
      mockup.deactive = false;
      mockup.f_demandelec5forecast_kw = last.f_demandelec5forecast_kw;
      mockup.f_demandelec5_kw = last.f_demandelec5_kw;
      mockup.f_fromeleckepco_kw = last.f_fromeleckepco_kw;
      mockup.f_prodelec_kw = last.f_prodelec_kw;
      mockup.f_savingcost_krw = Number(last.f_savingcost_krw).toFixed(1);

      if (last.f_demandelecall_kw) {
        mockup.selfsuff = Number(last.f_prodelec_kw / last.f_demandelecall_kw * 100).toFixed(1);
        if(mockup.selfsuff > 100) mockup.selfsuff = 100;
      } else {
        mockup.selfsuff = 100;
      }

      vm.selected = mockup;

      vm.buildings.push(mockup);

      vm.buildings.push({name:'101동', deactive: false});
      vm.buildings.push({name:'102동', deactive: false});
      vm.buildings.push({name:'103동', deactive: false});
      vm.buildings.push({name:'201동', deactive: true});
      vm.buildings.push({name:'202동', deactive: true});

      chart.load({
        json: data,
        keys: {
          value: ['f_prodelec_kw', 'f_demandelecall_kw']
        }
      })
    });
  }
})();
