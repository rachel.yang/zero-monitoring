/**
 * Created by rachel.yang on 2016. 2. 15..
 */
(function () {
  'use strict';

  angular
    .module('zeroWeb-monitoring')
    .controller('M4Controller', M4Controller);

  /** @ngInject */
  function M4Controller($log, c3) {
    $log.info("M4Controller start");
    //var vm = this;

    //m2 stacked bar chart
    c3.generate({
      bindto: '#m4_bar',
      data: {
        columns: [
          ['data1', 30, -200, 200, 400, 150, 250, 10, 20, 30, 140, 30, -200, -200, 400, 150, 250, 10, 20, 30, 40],
          ['data2', 30, -200, 200, 400, 150, 250, 10, 20, 30, 140, 30, -200, -200, 400, 150, 250, 10, 20, 30, 40],
          ['data3', 30, -200, 200, 400, 150, 250, 10, 20, 30, 40, 30, -200, -200, 400, 150, 250, 10, 20, 30, 40]
        ],
        type: 'bar',
        groups: [
          ['data1', 'data2', 'data3']
        ]
      },
      grid: {
        y: {
          lines: [{value:0}]
        }
      },
      size: {
        width: 1660,
        height: 300
      },
      bar: {
        width: {
          ratio: 0.3
        }
      },
      color: {
        pattern: ['#6fd7fb', '#49b8af', '#fec271']
      },
      legend: {
        show: false
      }
    });

  }
})();
