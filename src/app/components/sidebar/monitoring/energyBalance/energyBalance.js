/**
 * Created by rachel.yang on 2016. 1. 25..
 */
(function () {
  'use strict';

  angular
    .module('zeroWeb-monitoring')
    .controller('EnergyBalanceController', EnergyBalanceController);

  /** @ngInject */
  function EnergyBalanceController($log) {
    $log.info("EnergyBalanceController start");
    //var vm = this;
  }
})();
