/**
 * Created by yangyoomi on 2015. 12. 21..
 */
(function () {
  'use strict';

  angular
    .module('zeroWeb-monitoring', ['ui.bootstrap'])
    .config(monitoringRouterConfig)
    .controller('MonitoringController', MonitoringController);

  function monitoringRouterConfig($stateProvider) {
    $stateProvider
      .state('sidebar.monitoring', {
        abstract: true,
        templateUrl: 'app/components/sidebar/monitoring/monitoring.html',
        controller: 'MonitoringController',
        controllerAs: 'vm'
      })
      .state('sidebar.monitoring.m1', {
        url: '/m1',
        templateUrl: 'app/components/sidebar/monitoring/energyBalance/m1.html',
        controller: 'M1Controller',
        controllerAs: 'vm'
      })
      .state('sidebar.monitoring.m2', {
        url: '/m2',
        templateUrl: 'app/components/sidebar/monitoring/energyBalance/m2.html',
        controller: 'M2Controller',
        controllerAs: 'vm'
      })
      .state('sidebar.monitoring.m3', {
        url: '/m3',
        templateUrl: 'app/components/sidebar/monitoring/energyBalance/m3.html',
        controller: 'M3Controller',
        controllerAs: 'vm'
      })
      .state('sidebar.monitoring.m4', {
        url: '/m4',
        templateUrl: 'app/components/sidebar/monitoring/energyBalance/m4.html',
        controller: 'M4Controller',
        controllerAs: 'vm'
      })
      .state('sidebar.monitoring.m5', {
        url: '/m5',
        templateUrl: 'app/components/sidebar/monitoring/energyBalance/m5.html',
        controller: 'M5Controller',
        controllerAs: 'vm'
      })
    ;
  }

  /** @ngInject */
  function MonitoringController($log, $rootScope, store, $state) {
    $log.info("MonitoringController start");

    $("#sidebar-monitoring").addClass("site-detail-active");

    var vm = this;
    vm.loginUser = $rootScope.user;

    vm.logout = function() {
      $rootScope.user = "";
      store.storage.jwt = store.remove('jwt');
      $state.go("login");
    }
  }

})();
