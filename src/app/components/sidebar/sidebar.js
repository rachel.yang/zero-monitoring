/**
 * Created by rachel.yang on 2016. 2. 10..
 *
 * 나중에 모니터링 밑에 메뉴 생성을 위해 사이드바 안에 모니터링을 넣었음
 */
(function () {
  'use strict';

  angular
    .module('zeroWeb-sidebar',
      ['ui.bootstrap', 'zeroWeb-monitoring', 'zeroWeb-manage']
    )
    .config(sidebarRouterConfig)
    .controller('SidebarController', SidebarController);

  function sidebarRouterConfig($stateProvider) {
    $stateProvider
      .state('sidebar', {
        abstract: true,
        templateUrl: 'app/components/sidebar/sidebar.html',
        controller: 'SidebarController',
        controllerAs: 'vm'
      })
    ;
  }

  /** @ngInject */
  function SidebarController($log, $interval, moment) {
    $log.info("SidebarController start");

    var vm = this;

    $interval(function() {
      vm.currentDay = moment().format('MMMM Do YYYY');
      vm.currentTime = moment().format('a h:mm:ss');
    }, 1000);


  }

})();
