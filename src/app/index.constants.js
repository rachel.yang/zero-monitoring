/* global malarkey:true, moment:true, c3:true */
(function() {
  'use strict';

  angular
    .module('zeroWeb')
    .constant('malarkey', malarkey)
    .constant('moment', moment)
    .constant('c3', c3);
})();
